import { createStore } from "vuex";

const store = createStore({
  state: {
    cart: [],
  },
  mutations: {
    setCart: (state, product) => {
      const productExists = state.cart.some((item) => item.id === product.id);
      if (!productExists) {
        state.cart.push(product);
      }
    },
  },
  getters: {
    getCart: (state) => {
      return state.cart;
    },
  },
});

export default store;
